# Demo:
https://toble.bitbucket.io/

# Install required packages with npm:
npm install --save-dev typescript awesome-typescript-loader source-map-loader

npm install -g webpack

# Run webpack:
webpack --progress --colors --watch

# Editor:
http://alm.tools/
