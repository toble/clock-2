import { ClockState } from "./ClockState";
import { TimeMapper } from "./TimeMapper";

class F {
  static map(h: number, m: number): ClockState {
    return new ClockState(TimeMapper.mapHourToAngle(h), TimeMapper.mapHourToAngle(m));
  }

  static Blank: ClockState = F.map(7.5, 7.5);
  static HLine: ClockState = F.map(9, 3);
  static VLine: ClockState = F.map(6, 0);
  static AngleLU: ClockState = F.map(9, 0);
  static AngleRU: ClockState = F.map(3, 0);
  static AngleLD: ClockState = F.map(9, 6);
  static AngleRD: ClockState = F.map(3, 6);

}

export interface Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState;
}

export class Zero implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.VLine, F.AngleRD, F.AngleLD, F.VLine],
      [F.VLine, F.VLine, F.VLine, F.VLine],
      [F.VLine, F.VLine, F.VLine, F.VLine],
      [F.VLine, F.AngleRU, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    
    return data[yGridPos][xGridPos];
  }
}

export class One implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.AngleLD, F.Blank],
      [F.AngleRU, F.AngleLD, F.VLine, F.Blank],
      [F.Blank, F.VLine, F.VLine, F.Blank],
      [F.Blank, F.VLine, F.VLine, F.Blank],
      [F.AngleRD, F.AngleLU, F.AngleRU, F.AngleLD],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Two implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.VLine, F.AngleRD, F.HLine, F.AngleLU],
      [F.VLine, F.AngleRU, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Three implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Four implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.AngleLD, F.AngleRD, F.AngleLD],
      [F.VLine, F.VLine, F.VLine, F.VLine],
      [F.VLine, F.AngleRU, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.Blank, F.Blank, F.VLine, F.VLine],
      [F.Blank, F.Blank, F.AngleRU, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Five implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.VLine, F.AngleRD, F.HLine, F.AngleLU],
      [F.VLine, F.AngleRU, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Six implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.VLine, F.AngleRD, F.AngleLD, F.VLine],
      [F.VLine, F.AngleRU, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Seven implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.Blank, F.Blank, F.VLine, F.VLine],
      [F.Blank, F.Blank, F.VLine, F.VLine],
      [F.Blank, F.Blank, F.VLine, F.VLine],
      [F.Blank, F.Blank, F.AngleRU, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Eight implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.VLine, F.AngleRD, F.AngleLD, F.VLine],
      [F.map(0, 4.5), F.AngleRU, F.AngleLU, F.map(0, 7.5)],
      [F.map(6, 1.5), F.AngleRD, F.AngleLD, F.map(6, 10.5)],
      [F.VLine, F.AngleRU, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}

export class Nine implements Digit {
  getClockState(xGridPos: number, yGridPos: number): ClockState {
    let data: ClockState[][] = [
      [F.AngleRD, F.HLine, F.HLine, F.AngleLD],
      [F.VLine, F.AngleRD, F.AngleLD, F.VLine],
      [F.VLine, F.AngleRU, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.AngleLD, F.VLine],
      [F.AngleRD, F.HLine, F.AngleLU, F.VLine],
      [F.AngleRU, F.HLine, F.HLine, F.AngleLU]
    ];
    return data[yGridPos][xGridPos];
  }
}


let digits: Digit[] = [
  new Zero(),
  new One(),
  new Two(),
  new Three(),
  new Four(),
  new Five(),
  new Six(),
  new Seven(),
  new Eight(),
  new Nine(),
];

export default digits;