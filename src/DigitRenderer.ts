import { Digit, Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine } from "./Digit";
import { ClockTransition } from "./transitions/ClockTransition";
import { DigitTransition } from "./transitions/VariousSpeedDigitTransition";
import { ClockState } from "./ClockState";
import { Clock } from "./Clock";
import { DigitProvider } from "./DigitProvider";


export class DigitRenderer {
  private transitionTime: number = 300;
  private pauseTime: number = 200;

  private xPos: number;
  private yPos: number;
  private clockTransitions: ClockTransition[];
  private digitTransition: DigitTransition;
  private clockMap = [[]];
  private digitProvider: DigitProvider;
  nClockHorizontal: number = 4;
  nClockVertical: number = 6;;

  constructor(ctx: CanvasRenderingContext2D, pos: [number, number], size: [number, number], digits: DigitProvider) {
    this.xPos = pos[0];
    this.yPos = pos[1];
    let width = size[0];
    let height = size[1];
    this.digitProvider = digits;

    var horizontalRatio: number = width / (this.nClockHorizontal * 2);
    var verticalRatio: number = height / (this.nClockVertical * 2);
    var clockRadius: number = Math.min(horizontalRatio, verticalRatio);

    var clocks: Clock[] = [];

    for (let xGridPos = 0; xGridPos < this.nClockHorizontal; xGridPos++) {
      this.clockMap[xGridPos] = [];
      for (let yGridPos = 0; yGridPos < this.nClockVertical; yGridPos++) {
        let center: [number, number] = [xGridPos * clockRadius * 2 +
          clockRadius + pos[0], yGridPos * clockRadius * 2 + clockRadius + pos[1]];
        let clock = new Clock(ctx, clockRadius, center);
        this.clockMap[xGridPos][yGridPos] = clock;
        clocks.push(clock);
      }
    }

    this.currentTransition = new DigitTransition(this.digitProvider.getNextDigit(), this.digitProvider.getNextDigit());
  }

  private currentTransition: DigitTransition;
  render(counter: number) {
    if (this.hasTransitionEnded(counter)) {
        this.currentTransition = new DigitTransition(this.currentTransition.endState, this.digitProvider.getNextDigit());
    }
    let progress: number = ((counter % (this.transitionTime + this.pauseTime))) / this.transitionTime;
    progress = Math.min(progress, 1);

    for (let xGridPos = 0; xGridPos < this.nClockHorizontal; xGridPos++) {
      for (let yGridPos = 0; yGridPos < this.nClockVertical; yGridPos++) {
        let clock: Clock = this.clockMap[xGridPos][yGridPos];
        let clockState: ClockState = this.currentTransition.getClockStateOfClock([xGridPos, yGridPos], progress);
        clock.renderClock(clockState);
      }
    }
  }

  hasTransitionEnded(counter: number): boolean {
    return counter % (this.transitionTime + this.pauseTime) == 0 && counter > 0;
  }
}