import { ClockState } from "./ClockState";

export class Clock {
  private context: CanvasRenderingContext2D;
  private radius: number;
  private innerRadiusFactor: number = 0.97;
  private center: [number, number];

  constructor(context: CanvasRenderingContext2D, radius: number, center: [number, number]) {
    this.context = context;
    this.radius = radius;
    this.center = center;
  }

  render(counter: number) {

  }

  renderClock(clockState: ClockState) {
    this.drawOuterBorder();
    this.drawCenter();
    this.drawHands(clockState);
  }

  drawOuterBorder() {
    this.context.lineWidth = 1
    this.context.beginPath();
    this.context.arc(this.center[0], this.center[1], this.radius * this.innerRadiusFactor, 0, 2 * Math.PI);
    this.context.stroke();
  }

  drawCenter() {
    this.context.lineWidth = 1
    this.context.beginPath();
    this.context.arc(this.center[0], this.center[1], 1.5, 0, 2 * Math.PI);
    this.context.stroke();
  }

  drawHands(state: ClockState) {
    this.drawHand(this.radius * 0.9, state.getMinuteAngle());
    this.drawHand(this.radius * 0.85, state.getHourAngle());
  }


  drawHand(radius: number, angle: number): void {
    this.context.beginPath();
    this.context.lineWidth = 4
    this.context.moveTo(this.center[0], this.center[1]);
    let upAlignedAngle: number = angle - Math.PI / 2;
    this.context.arc(this.center[0], this.center[1], radius, upAlignedAngle, upAlignedAngle);
    this.context.stroke();
  }
}
