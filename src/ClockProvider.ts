import { DigitProvider } from "./DigitProvider";
import { Digit } from "./Digit";
import digits from "./Digit";
import { One } from "./Digit";
export class ClockProvider {
  private clock: Clock = new Clock();

  private getProviderForClockFunction(clockFun: () => number): DigitProvider {
    let provider: DigitProvider = {
      getNextDigit(): Digit {
        return digits[clockFun()];
      }
    };
    return provider;
  }

  getDigitsForH1(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getH1());
  }

  getDigitsForH2(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getH2());
  }

  getDigitsForM1(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getM1());
  }

  getDigitsForM2(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getM2());
  }

  getDigitsForS1(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getS1());
  }
  getDigitsForS2(): DigitProvider {
    return this.getProviderForClockFunction(() => this.clock.getS2());
  }
}

class Clock {

  getH2(): number {
    return new Date().getHours() % 10;
  }

  getH1(): number {
    return (new Date().getHours() - this.getH2()) / 10;
  }

  getM1(): number {
    return (new Date().getMinutes() - this.getM2()) / 10;
  }

  getM2(): number {
    return new Date().getMinutes() % 10;
  }

  getS1(): number {
    return (new Date().getSeconds() - this.getS2()) / 10;
  }

  getS2(): number {
    return new Date().getSeconds() % 10;
  }
}