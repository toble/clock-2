export class ClockState {
  private minAngle: number; 
  private hourAngle: number;
  constructor(hourAngle: number, minAngle: number) { 
    this.hourAngle = hourAngle;
    this.minAngle = minAngle;
  }

  getHourAngle(): number { 
    return this.hourAngle;
  }
  getMinuteAngle(): number { 
    return this.minAngle;
  }
}