export class TimeMapper {
  static mapHourToAngle(hour: number): number {
    return 2 * Math.PI / 12 * hour;
  }

  static mapMinuteToAngle(minute: number): number {
    return 2* Math.PI /60 * minute;
  }
}