import { Digit } from "./Digit";

export interface DigitProvider {
  getNextDigit() : Digit;
}