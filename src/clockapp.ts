import { Clock } from "./Clock";
import { Digit, Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine } from "./Digit";
import digits from "./Digit";
import { DigitRenderer } from "./DigitRenderer";
import { ClockProvider } from "./ClockProvider";

let c = <HTMLCanvasElement>document.getElementById("clock");
let ctx = c.getContext("2d");
let width: number = ctx.canvas.width;
let height: number = ctx.canvas.height

ctx.strokeRect(0, 0, width, height);

let nHorzDigits = 6;
let nVertDigits = 1;
let digitWidth = width / nHorzDigits;
let digitSize : [number, number] = [digitWidth, height / nVertDigits];

let clockProvider = new ClockProvider();

let rendererH1: DigitRenderer = new DigitRenderer(ctx, [0, 0], digitSize, clockProvider.getDigitsForH1());
let rendererH2: DigitRenderer = new DigitRenderer(ctx, [digitWidth * 1, 0], digitSize, clockProvider.getDigitsForH2());

let rendererM1: DigitRenderer = new DigitRenderer(ctx, [digitWidth * 2, 0], digitSize, clockProvider.getDigitsForM1());
let rendererM2: DigitRenderer = new DigitRenderer(ctx, [digitWidth * 3, 0], digitSize, clockProvider.getDigitsForM2());

let rendererS1: DigitRenderer = new DigitRenderer(ctx, [digitWidth * 4, 0], digitSize, clockProvider.getDigitsForS1());
let rendererS2: DigitRenderer = new DigitRenderer(ctx, [digitWidth * 5, 0], digitSize, clockProvider.getDigitsForS2());
console.log(rendererH1);
console.log(rendererH2);
console.log(rendererM1);
console.log(rendererM2);
console.log(rendererS1);
console.log(rendererS2);

let infoField = document.getElementById("info");

var counter = 0;
let fps = 120;
let frameTime = 1000 / fps;

function render() {
  ctx.clearRect(0, 0, width, height);
  //infoField.innerText = "Time: " + Math.trunc(counter / fps) + " Counter: " + counter;
  rendererH1.render(counter);
  rendererH2.render(counter);
  rendererM1.render(counter);
  rendererM2.render(counter);
  rendererS1.render(counter);
  rendererS2.render(counter);

  counter++;
}

window.setInterval(render, frameTime);

