import { Digit } from "./../Digit";
import { Clock } from "./../Clock";
import { ClockState } from "./../ClockState";
import { ClockTransition } from "./ClockTransition";
export class DigitTransition {
  endState: Digit;
  startState: Digit;

  constructor(startState: Digit, endState: Digit) {
    this.startState = startState;
    this.endState = endState;
  }

  getClockStateOfClock(gridPos: [number, number], progress: number): ClockState {
    let endClockState: ClockState = this.endState.getClockState(gridPos[0], gridPos[1]);
    let farClockState = new ClockState(endClockState.getHourAngle() + 2 * Math.PI, endClockState.getMinuteAngle() + 4 * Math.PI);
    let clockTransition: ClockTransition = new ClockTransition(this.startState.getClockState(gridPos[0], gridPos[1]), farClockState);

    return clockTransition.calculateInterpolatedState(progress);
  }
}
