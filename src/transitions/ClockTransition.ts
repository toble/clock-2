import { ClockState } from "./../ClockState";

export class ClockTransition {
  private endState: ClockState;
  private startState: ClockState;

  constructor(startState: ClockState, endState: ClockState) {
    this.startState = startState;
    this.endState = endState;
  }

  calculateInterpolatedState(progress: number): ClockState {
    return new ClockState(
      this.calculateInterpolatedAngle(this.startState.getHourAngle(), this.endState.getHourAngle(), progress),
      this.calculateInterpolatedAngle(this.startState.getMinuteAngle(), this.endState.getMinuteAngle(), progress),
    );
  }

  private calculateInterpolatedAngle(a1: number, a2: number, progress: number) {
    //console.log("a1: " + a1 + " a2: " + a2 + " progress: " + progress);
    return a1 + (a2 - a1) * progress;
  }
}