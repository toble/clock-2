import { Digit } from "./../Digit";
import { Clock } from "./../Clock";
import { ClockState } from "./../ClockState";
import { ClockTransition } from "./ClockTransition";
export class DigitTransition {
  endState: Digit;
  startState: Digit;
  private speedVarations = [[]];
  private minDir = -1;
  private hDir = 1;
  constructor(startState: Digit, endState: Digit) {
    this.startState = startState;
    this.endState = endState;
  }

  getClockStateOfClock(gridPos: [number, number], progress: number): ClockState {
    if (typeof this.speedVarations[gridPos[0]] === 'undefined') {
      this.speedVarations[gridPos[0]] = [];
    }
    if (typeof this.speedVarations[gridPos[0]][gridPos[1]] === 'undefined') {
      this.speedVarations[gridPos[0]][gridPos[1]] = 1 + Math.random() / 2;
    }

    let endClockState: ClockState = this.endState.getClockState(gridPos[0], gridPos[1]);
    let farClockState = new ClockState(endClockState.getHourAngle() + 2 * Math.PI * this.hDir, endClockState.getMinuteAngle() + 2 * Math.PI * this.minDir);
    let clockTransition: ClockTransition = new ClockTransition(this.startState.getClockState(gridPos[0], gridPos[1]), farClockState);

    let cappedProgress: number = Math.min(1, progress * this.speedVarations[gridPos[0]][gridPos[1]]);
    return clockTransition.calculateInterpolatedState(cappedProgress);
  }
}
